import os

while True:
	command = raw_input('Input command: ')

	pid = os.fork()
	
	split = command.split(' ')
	if pid == 0:
		os.execvp(split[0], split)
