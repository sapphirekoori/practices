let size = 30000000;
let array1 = [];
let start1 = new Date();

for (let i = 0; i < size; ++i) {
    array1.push(i);
}

let end1 = new Date();
var diff1 = end1.getTime() - start1.getTime();

let array2 = new Array(size);
let start2 = new Date();

for (let i = 0; i < size; ++i) {
    array2[i] = i;
}

let end2 = new Date();
var diff2 = end2.getTime() - start2.getTime();

console.log(diff1);
console.log(diff2);