let getNewObject = () => {
    return { bar: 1, foo: 2 };
};

let size = 10000000;
let array1 = [];
let start1 = new Date();

for (let i = 0; i < size; ++i) {
    array1.push(getNewObject());
}

let end1 = new Date();
var diff1 = end1.getTime() - start1.getTime();

let array2 = new Array(size);
let start2 = new Date();

for (let i = 0; i < size; ++i) {
    array2[i] = getNewObject();
}

let end2 = new Date();
var diff2 = end2.getTime() - start2.getTime();

let array3 = [];
array3.length = size;
let start3 = new Date();

for (let i = 0; i < size; ++i) {
    array2[i] = getNewObject();
}

let end3 = new Date();
var diff3 = end3.getTime() - start3.getTime();

console.log(diff1);
console.log(diff2);
console.log(diff3);