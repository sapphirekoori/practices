import user from './../user/user';
import schedule from './schedule';

exports.get = async (req, res, next) => {
    try {
        if (!req.body.scheduleId) {
            res.status(422).send({ message: 'Empty schedule Id.' });
        }

        let matchedSchedule = await schedule.get(req.body.scheduleId);

        if (matchedSchedule) {
            res.status(200).send({ data: matchedSchedule });
        }
        else {
            res.status(404).send({ message: 'Schedule not found.' });
        }
    }
    catch (err) {
        console.log('get schedule error.', err);

        res.status(500).send({ message: 'Unexpected error.' });
    }
};

exports.getScheduleByUser = async (req, res, next) => {
    try {
        if (!req.body.user) {
            res.status(422).send({ message: 'Empty user name.' });
            return;
        }

        let matchedUser = await user.get(req.body.user);

        if (!matchedUser) {
            res.status(404).send({ message: 'User not found.' });
            return;
        }

        let schedules = await schedule.getScheduleByUser(matchedUser, req.body.date);

        res.status(200).send({ data: schedules });
    }
    catch (err) {
        console.log('get schedule by user error.', err);

        res.status(500).send({ message: 'Unexpected error.' });
    }
};

exports.create = async (req, res, next) => {
    try {
        if (!req.body.user) {
            res.status(422).send({ message: 'Empty user name.' });
            return;
        }

        if (!req.body.from || !req.body.to || !req.body.description) {
            res.status(422).send({ message: 'Invalid schedule.' });
            return;
        }

        let matchedUser = await user.get(req.body.user);

        if (!matchedUser) {
            res.status(404).send({ message: 'User not found.' });
            return;
        }

        let newSchedule = {
            from: req.body.from,
            to: req.body.to,
            description: req.body.description
        };

        let created = await schedule.create(newSchedule, matchedUser);

        res.status(200).send({ data: created });
    }
    catch (err) {
        console.log('create schedule error.', err);

        res.status(500).send({
            message: 'Unexpected error.'
        });
    }
};