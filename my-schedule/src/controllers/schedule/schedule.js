import mongoose from 'mongoose'

const scheduleModel = mongoose.model('Schedule');

let get = async (scheduleId) => {
    if (!scheduleId) {
        return null;
    }

    let schedule = await scheduleModel.findById(mongoose.Types.ObjectId(scheduleId));
    return schedule;
};

let getScheduleByUser = async (user, date) => {
    if (!user) {
        return null;
    }

    let query = {
        $and: []
    };

    query.$and.push({ 'user': user._id });

    if (date) {
        let dateObject = new Date(date);
        let nextDate = dateObject.setDate(dateObject.getDate() + 1);;

        query.$and.push({
            $or: [ { 'for': { $gte: date, $lt: nextDate } }, { 'to': { $gte: date, $lt: nextDate } } ]
        });
    }

    let schedules = await scheduleModel.find(query);
    return schedules;
};

let create = async (schedule, user) => {
    if (!schedule || !user) {
        return null;
    }

    let newSchedule = scheduleModel({
        from: schedule.from,
        to: schedule.to,
        description: schedule.description,
        user: user._id
    });

    await newSchedule.save();

    return newSchedule;
};

exports.get = get;
exports.getScheduleByUser = getScheduleByUser;
exports.create = create;