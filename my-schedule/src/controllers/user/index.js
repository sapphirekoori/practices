import user from './user'

exports.get = async (req, res, next) => {
    try {
        if (!req.body.name) {
            res.status(422).send({ message: 'Empty user name.' });
            return;
        }

        let matchedUser = await user.get(req.body.name);

        if (matchedUser) {
            res.status(200).send({
                data: matchedUser
            });
        }
        else {
            res.status(404).send({ message: 'User not found.' });
        }
    }
    catch (err) {
        console.log('get user error.', err);

        res.status(500).send({ message: 'Unexpected error.' });
    }
}

exports.create = async (req, res, next) => {
    try {
        let userName = req.body.name;

        if (!userName) {
            res.status(422).send({ message: 'Empty user name.' });
            return;
        }
    
        let existingUser = await user.get(userName);
    
        if (existingUser) {
            res.status(409).send({ message: `User ${ userName } already exists.` });
        }
        else {
            let newUser = await user.create(userName);

            res.status(200).send({ data: newUser });
        }
    }
    catch (err) {
        console.log('user creation error.', err);

        res.status(500).send({ message: 'Unexpected error.' });
    }
};