import mongoose from 'mongoose';

const user = mongoose.model('User');

let get = async (userName) => {
    if (!userName) {
        return null;
    }

    let matchedUser = await user.findOne({ name: userName });

    return matchedUser;
};

let update = async (userName) => {
    let existingUser = await get(userName);

    if (!existingUser) {
        return null;
    }

    existingUser.name = userName;
    await existingUser.save();

    return existingUser;
};

let create = async (userName) => {
    let newUser = user({
        name: userName
    });

    await newUser.save();

    return newUser;
};

exports.get = get;
exports.update = update;
exports.create = create;