import mongoose from 'mongoose';

import dbConfig from './../config/database';

mongoose.Promise = global.Promise;

mongoose.connect(
    dbConfig.url,
    {
        useNewUrlParser: true,
        keepAlive: true,
        reconnectTries: 10
    },
    (err, db) => {
        if (err) {
            console.log('Failed to connect mongodb.', err);
        }
        else {
            console.log('Connected to mongodb.');
        }
    }
);

require('./../models/user');
require('./../models/schedule');