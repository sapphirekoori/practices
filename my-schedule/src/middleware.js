import compression from 'compression';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from 'morgan';
import fs from 'fs';

import path from 'path';

module.exports = app => {
    app.use((req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
        res.header('Access-Control-Allow-Headers', 'Content-type,Accept,x-access-token,X-Key');
        if (req.method == 'OPTIONS') {
            res.status(200).end();
        }
        else {
            next();
        }
    });

    app.use(cors());
    app.use(compression());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    let accessLogStream = fs.createWriteStream(
        path.join(__dirname, 'access.log'),
        { flags: 'a' });
    
    app.use(morgan('short', { stream: accessLogStream }));

    // TODO: express-jwt

    /*app.use(
        jwt({ secret: '' })
    );

    app.use((err, req, res, next) => {
        if (err.name === 'UnauthorizedError') {
            res.status(401).send('Missing authentication credentials.');
        }
    });*/
};