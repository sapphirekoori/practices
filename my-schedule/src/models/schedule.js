import mongoose from 'mongoose';

const Schema = mongoose.Schema;

let schedule = Schema({
    from: { type: Date, required: true },
    to: { type: Date, required: true, },
    description: { type: String, required: true },
    user: [{ type: Schema.Types.ObjectId, ref: 'User' }]
});

mongoose.model('Schedule', schedule);