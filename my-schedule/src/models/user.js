import mongoose from 'mongoose';
import timestamps from 'mongoose-timestamp';

let user = mongoose.Schema({
    name: { type: String, required: true }
});

mongoose.plugin(timestamps);

mongoose.model('User', user);