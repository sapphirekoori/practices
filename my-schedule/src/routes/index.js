import usersRouter from './user/index'
import schedulesRouter from './schedule/index';

module.exports = app => {
    app.use('/users', usersRouter);
    app.use('/schedules', schedulesRouter);
};