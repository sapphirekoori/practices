import express from 'express';

import schedulerController from '../../controllers/schedule/index';

let router = express.Router();

router.get('/', schedulerController.get);
router.post('/', schedulerController.create);
router.get('/user/', schedulerController.getScheduleByUser);

module.exports = router;