import express from 'express';

import userController from '../../controllers/user/index';

let router = express.Router();

router.get('/', userController.get);
router.post('/', userController.create);

module.exports = router;