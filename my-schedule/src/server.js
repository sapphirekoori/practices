import express from 'express';

import config from './config/server';

const app = express();

require('./database/index');

// middlewares

require('./middleware')(app);

require('./routes/index')(app);

let port = process.env.port || config.port;
app.listen(port, () => {
    console.log(`Started on port ${ port }.`);
});