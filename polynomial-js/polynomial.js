class Polynomial {
    constructor(coefficients) {
        this.coefficients = this.formatCoefficients(coefficients);
    }

    formatCoefficients(coefficients) {
        while (coefficients[coefficients.length - 1] == 0) {
            coefficients.pop();
        }
        return coefficients;
    }

    getExpression() {
        if (this.coefficients.length == 0) {
            return '0';
        }

        let indeterminate = 'x';
        let notation = '';
        for (let i = 0; i < this.coefficients.length; ++i) {
            if (this.coefficients[i] == 0) {
                continue;
            }

            let sign = this.coefficients[i] > 0 ? '+' : '';
            if (i == 0) {
                notation = sign + this.coefficients[i];
            }
            else {
                let term = sign + (this.coefficients[i] == 1 ? '' : this.coefficients[i]) + indeterminate + (i == 1 ? '' : ('^' + i));
                notation = term + notation;
            }
        }

        if (notation.charAt(0) == '+') {
            notation = notation.substr(1);
        }

        return notation;
    }

    print() {
        console.log(this.getExpression());
    }

    getCoefficient(degree) {
        if (degree < this.coefficients.length) {
            return this.coefficients[degree];
        }
        else {
            return 0;
        }
    }

    getDegree() {
        return this.coefficients.length == 0 ? 0 : (this.coefficients.length - 1);
    }

    add(addend) {
        let maxLength = Math.max(this.coefficients.length, addend.coefficients.length);

        for (let i = 0; i < maxLength; ++i) {
            if (!this.coefficients[i]) {
                this.coefficients[i] = 0;
            }
            this.coefficients[i] += addend.coefficients[i] ? addend.coefficients[i] : 0;
        }

        this.coefficients = this.formatCoefficients(this.coefficients);
    }

    substract(subtrahend) {
        let maxLength = Math.max(this.coefficients.length, subtrahend.coefficients.length);

        for (let i = 0; i < maxLength; ++i) {
            if (!this.coefficients[i]) {
                this.coefficients[i] = 0;
            }
            this.coefficients[i] -= subtrahend.coefficients[i] ? subtrahend.coefficients[i] : 0;
        }

        this.coefficients = this.formatCoefficients(this.coefficients);
    }

    multiply(multiplicand) {
        let newCoefficients = [];
        for (let i = 0; i < this.coefficients.length; ++i) {
            for (let j = 0; j < multiplicand.coefficients.length; ++j) {
                if (!newCoefficients[i + j]) {
                    newCoefficients[i + j] = 0;
                }
                newCoefficients[i + j] += this.coefficients[i] * multiplicand.coefficients[j];
            }
        }
        this.coefficients = newCoefficients;
    }

    divide(divisor) {
        // TODO:
        // if (divisor.coefficients.length > this.coefficients.length) {
        //     return [];
        // }
        // let quotient = [];
        // while (this.coefficients[this.divisor.length - 1] != 0) {
        //     let 
        // }
    }
}