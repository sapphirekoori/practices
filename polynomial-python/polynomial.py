class Polynomial:
    def __init__(self, coefficients):
        self.coefficients = coefficients

    def __format_coefficients(self):
        while self.coefficients[len(self.coefficients) - 1] == 0:
            self.coefficients.pop()

    def __get_expression(self):
        return self.get_expression()

    def __get_degree(self):
        if len(self.coefficients) == 0:
            return 0
        else:
            return len(self.coefficients) - 1

    def get_expression(polynomial):
        if len(polynomial.coefficients) == 0:
            return '0'

        indetermine = 'x'
        expression = ''

        for i in range(len(polynomial.coefficients)):
            if polynomial.coefficients[i] == 0:
                continue

            sign = ''
            if polynomial.coefficients[i] > 0:
                sign = '+'
            elif i > 0:
                sign = '-'

            if i == 0:
                expression = sign + str(polynomial.coefficients[i])
            else:
                term = sign
                if polynomial.coefficients[i] > 1:
                    term += str(polynomial.coefficients[i])
                term += indetermine
                if i > 1:
                    term += '^' + str(i)

                expression = term + expression

        return expression.lstrip('+')

    def add_polynomial(self, addend):
        self.coefficients = self.add(self, addend).coefficients # ??

    @staticmethod
    def add(polynomial1, polynomial2):
        max_length = max(len(polynomial1.coefficients), len(polynomial2.coefficients))
        new_polynomial = Polynomial([0] * max_length)

        for i in range(max_length):
            if i < len(polynomial1.coefficients):
                new_polynomial.coefficients[i] = polynomial1.coefficients[i]

            if i < len(polynomial2.coefficients):
                new_polynomial.coefficients[i] += polynomial2.coefficients[i]

        new_polynomial.__format_coefficients()
        return new_polynomial

    def substract_polynomial(self, subtrahend):
        self.coefficients = self.substract(self, subtrahend).coefficients # ??

    @staticmethod
    def substract(polynomial1, polynomial2):
        max_length = max(len(polynomial1.coefficients), len(polynomial2.coefficients))
        new_polynomial = Polynomial([0] * max_length)

        for i in range(max_length):
            if i < len(polynomial1.coefficients):
                new_polynomial.coefficients[i] = polynomial1.coefficients[i]

            if i < len(polynomial2.coefficients):
                new_polynomial.coefficients[i] -= polynomial2.coefficients[i]

        new_polynomial.__format_coefficients()
        return new_polynomial

    def multiply_polynomial(self, multiplicand):
        self.coefficients = self.multiply(self, multiplicand).coefficients

    @staticmethod
    def multiply(multiplier, multiplicand):
        new_polynomial = Polynomial([0] * (len(multiplier.coefficients) + len(multiplicand.coefficients) - 1))

        for i in range(len(multiplier.coefficients)):
            for j in range(len(multiplicand.coefficients)):
                new_polynomial.coefficients[i + j] += multiplier.coefficients[i] * multiplicand.coefficients[j]

        new_polynomial.__format_coefficients()
        return new_polynomial

p1 = Polynomial([1, 2, 3])
p2 = Polynomial([2, 3, 4])
# print(p1.get_expression())
# print(p2.get_expression())

p1.add_polynomial(p2)
# p1.substract_polynomial(p2)
print(p1.get_expression())