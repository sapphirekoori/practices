class ListNode {
    constructor(value) {
        this.value = value;
        this.previous = null;
        this.next = null;
    }
}

class Queue {
    constructor() {
        this.head = null;
        this.end = null;
        this.length = 0;
    }

    enqueue(value) {
        let newNode = new ListNode(value);
        if (!this.head) {
            this.head = newNode;
        }

        if (this.end) {
            this.end.next = newNode;
        }
        this.end = newNode;
        ++this.length;
    }

    dequeue() {
        if (!this.head) {
            return null;
        }

        let removed = this.head;
        this.head = removed.next;
        --this.length;
        return removed.value;
    }

    getSize() {
        return this.length;
    }

    getAllValues() {
        let curr = this.head;
        let values = [];
        while (curr) {
            values.push(curr.value);
            curr = curr.next;
        }
        return values;
    }
}
