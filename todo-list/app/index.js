// import '.scss' files
import './scss/styles.scss';

import TodoList from './lib/todoList';

(function(window) {
    let _init = () => {
        var todoList = new TodoList();
        todoList.init();
    };

    window.Todo = {
        init: _init
    };
}) (window);