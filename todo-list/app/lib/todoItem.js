export default class TodoItem {
    constructor() {
        this.id = 0;
        this.description = null;
        this.done = false;
        this.lastUpdate = new Date();
    }
}