import TodoItem from './todoItem';

const TODO_STORAGE = 'TODO_STORAGE';
const TODO_ITEM_DEFAULT_TEXT = 'Click to modify';

export default class TodoList {
    // define class member in constructor
    constructor() {
        this.lastId = 1;
        this.todoList = [];
        this.expanded = false;

        this.todoDiv = null;
        this.todoButton = null;
        this.todoItemList = null;
    }

    init() {
        if (typeof Storage === 'underfined') {
            console.warn('Storage is not supported for this browser.');
            return;
        }

        this.loadFromStorage();
        this.render();
    }

    updateToStorage() {
        // update to local storage
        // should update so frequently ?
        if (this.todoList.length == 0) {
            return;
        }

        let todoData = [];

        if (this.todoItemList) {
            this.todoItemList.childNodes.forEach((item, index) => {
                let span = item.getElementsByTagName('span')[0];
                let isDone = new RegExp('(\\s|^)done(\\s|$)'.test);
    
                if (!span.innerText || span.innerText == TODO_ITEM_DEFAULT_TEXT) {
                    return;
                }
    
                let todoItem = new TodoItem();
                todoItem.id = index;
                todoItem.description = span.innerText;
                todoItem.done = span.className.split(/\s+/g).indexOf('done') > -1;
    
                todoData.push(todoItem);
            });
    
            window.localStorage.setItem(TODO_STORAGE, JSON.stringify(todoData));
        }
    }

    loadFromStorage() {
        let storage = window.localStorage.getItem(TODO_STORAGE);

        if (storage != null) {
            this.todoList = JSON.parse(storage);

            if (this.todoList != null) {
                this.todoList.forEach((item, index) => {
                    item.id = index;
                });

                this.lastId = this.todoList.length - 1;
            }
        }
    }

    // use vanilla js to init the elements
    render() {
        this.todoDiv = document.createElement('div');
        this.todoDiv.id = 'todo-div';

        this.todoButton = document.createElement('div');
        this.todoButton.id = 'todo-sidebar-button';

        this.todoDiv.appendChild(this.todoButton);
        this.todoDiv.appendChild(this.generateMenu());

        this.todoButton.addEventListener('click', () => { this.toggle() });

        // render item list
        let itemDivs = this.todoList.map(item => this.getItemDiv(item));
        itemDivs.forEach(e => { this.todoItemList.appendChild(e); });

        document.body.appendChild(this.todoDiv);
    }

    generateMenu() {
        let todoMenu = document.createElement('div');
        todoMenu.id = 'todo-menu';

        let todoHeader = document.createElement('div');
        todoHeader.id = 'todo-header';

        let desc = document.createElement('span');
        desc.innerHTML = 'To Do List';

        let sortButton = document.createElement('div');
        sortButton.className = 'sort';
        sortButton.innerText = 'Sort by Date';
        sortButton.setAttribute('aria-label', 'sort');

        let addButton = document.createElement('div');
        addButton.className = 'button';
        addButton.innerHTML = '&#10010;';
        addButton.setAttribute('aria-label', 'add');

        addButton.addEventListener('click', () => { this.addItem() });

        let quitButton = document.createElement('div');
        quitButton.className = 'button';
        quitButton.innerHTML = '&#10008;';
        quitButton.setAttribute('aria-label', 'quit');

        quitButton.addEventListener('click', () => {
            let todoDiv = document.getElementById('todo-div');
            document.body.removeChild(todoDiv);

            // TODO:
        });

        todoHeader.appendChild(desc);
        todoHeader.appendChild(quitButton);
        todoHeader.appendChild(addButton);

        this.todoItemList = document.createElement('div');
        this.todoItemList.id = 'todo-item-list';

        todoMenu.appendChild(todoHeader);
        todoMenu.appendChild(this.todoItemList);

        return todoMenu;
    }

    // add an item
    addItem() {
        // add the item to todoList
        let item = new TodoItem();
        item.id = ++this.lastId;
        item.description = null;

        this.todoList.push(item);

        // update UI
        this.todoItemList.appendChild(this.getItemDiv(item));
        // TODO:
        
        this.todoItemList.scrollTop = this.todoItemList.scrollHeight;
    }

    // remove an item
    removeItem(id) {
        // remove an item from todoList
        this.todoList = this.todoList.filter(t => t.id !== id);

        let item = this.getChildNodeFromDiv(id);
        if (item) {
            // TODO: remove event listener
            this.todoItemList.removeChild(item);
            this.updateToStorage();
        }
    }

    // mark an item as done
    markDone(id) {
        let todoItem = this.todoList.filter(t => t.id == id)[0];
        todoItem.done = true;
        
        let item = this.getChildNodeFromDiv(id);
        if (item) {
            let span = item.getElementsByTagName('span')[0];
            span.classList.add('done');
            this.updateToStorage();
        }
    }

    getChildNodeFromDiv(id) {
        for (let i = 0; i < this.todoItemList.childNodes.length; i++) {
            let currItem = this.todoItemList.childNodes[i];
            let currId = parseInt(currItem.getAttribute('data-index'), 10);

            if (currId == id) {
                return currItem;
            }
        }

        return null;
    }

    // expand/collapse the todo list
    // use vanilla js only
    toggle() {
        var todoDiv = this.todoDiv;
        var start = 0;
        var end = 0;
        var step = 10;

        if (this.expanded) {
            // collapse
            start = 0;
            end = -270;
            step = -step;
        }
        else {
            // expand
            start = -270;
            end = 0;
        }

        let interval = setInterval(function() {
            if (start == end) {
                clearInterval(interval);
            }
            else {
                start += step;
                todoDiv.style.right = start + 'px';
            }
        }, 5);

        this.expanded = !this.expanded;
    }

    getItemDiv(item) {
        /*
        final product
        <div class="todo-item-wrapper">
            <div class="desc">
                <span>TEXT</span>
                <input type="text" class="" />
            </div>
            <div class="button">&#10008;</div> <!-- close -->
            <div class="button">&#10004;</div> <!-- tick -->
        </div>
        */
        let div = document.createElement('div');
        div.className = 'todo-item-wrapper';
        div.setAttribute('data-index', item.id);

        let divDesc = document.createElement('div');
        divDesc.className = 'desc';

        let desc = document.createElement('span');
        if (item.description) {
            desc.innerText = item.description;
            desc.title = item.description;
        }
        else {
            desc.innerText = TODO_ITEM_DEFAULT_TEXT;
            desc.className = 'empty';
        }

        if (item.done) {
            desc.className = 'done';
        }

        let textInput = document.createElement('input');
        textInput.setAttribute('type', 'text');

        var endInputMode = () => {
            let inputValue = textInput.value;
            textInput.classList.remove('edit');

            let span = textInput.previousSibling;
            span.style.display = 'block';
            span.innerText = inputValue;

            // if input is empty, leave as default
            if (inputValue) {
                span.innerText = inputValue;
                span.title = inputValue;
                span.classList.remove('empty');
            }
            else {
                span.innerText = TODO_ITEM_DEFAULT_TEXT;
                span.title = '';
                span.classList.add('empty');
            }

            // need to update list
            this.updateToStorage();
        }

        textInput.addEventListener('focusout', () => {
            endInputMode();
        });

        textInput.addEventListener('keypress', (e) => {
            if (e.keyCode == 13) { // ENTER
                endInputMode();
            }
        });

        desc.addEventListener('click', (e) => {
            if (!desc.hidden) {
                desc.style.display = 'none';

                let input = desc.nextSibling;
                input.className = 'edit';

                if (desc.innerText != TODO_ITEM_DEFAULT_TEXT) {
                    input.value = desc.innerText;
                }
                
                input.focus();
            }
        });

        divDesc.appendChild(desc);
        divDesc.appendChild(textInput);

        let editButton = document.createElement('div');

        let tickbutton = document.createElement('div');
        tickbutton.className = 'button';
        tickbutton.innerHTML = '&#10004;';
        tickbutton.setAttribute('aria-label', 'done');

        tickbutton.addEventListener('click', () => { this.markDone(item.id); });

        let closeButton = document.createElement('div');
        closeButton.className = 'button';
        closeButton.innerHTML = '&#10008;';
        closeButton.setAttribute('aria-label', 'close');

        closeButton.addEventListener('click', () => { this.removeItem(item.id); });

        var todoList = this.todoList;
        var todoItems = this.todoItemList;

        div.appendChild(divDesc);
        div.appendChild(closeButton);
        div.appendChild(tickbutton);

        return div;
    }
}