
const urlShortener = require('./urlShortener');

const mongoose = require('mongoose');
const shortenedUrl = mongoose.model('ShortenedUrl');

module.exports = {
    getUrl: async (shortened) => {
        let item = await shortenedUrl.findOne({ shortened: shortened });
        return item;
    },
    addUrl: async (original) => {
        let item = await shortenedUrl.findOne({ original: original });

        if (!item) {
            // insert one

            let found = false;

            while (!found) {
                let code = urlShortener.generate();

                existingItem = await shortenedUrl.findOne({ shortened: code });

                if (!existingItem) {
                    item = new shortenedUrl({
                        original: original,
                        shortened: code
                    });
        
                    await item.save();
                }
            }
        }
        
        return item;
    }
};