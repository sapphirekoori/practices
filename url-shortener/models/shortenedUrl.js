var mongoose = require('mongoose');

var shortenedUrl = mongoose.Schema({
    original: String,
    shortened: String,
    createdAt: { type: Date, default: Date.now }
});

mongoose.model('ShortenedUrl', shortenedUrl);