var path = require('path');
let urlCollection = require('./../app/urlCollection');

module.exports = app => {
    app.get('/', (req, res) => {
        res.sendFile(path.resolve(__dirname + './../public/index.html'));
    });

    app.get('/:code', async (req, res) => {
        console.log(`Get URL for ${ req.params.code } ...`);

        let url = await urlCollection.getUrl(req.params.code);
        return res.redirect(url ? url.original : 'http://localhost:3000');
    });
};