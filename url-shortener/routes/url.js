const urlCollection = require('./../app/urlCollection');
const validUrl = require('valid-url');

module.exports = app => {
    app.get('/api/url/:code', async (req, res) => {
        console.log(`Get URL for ${ req.params.code } ...`);

        let url = await urlCollection.getUrl(req.params.code);
        return res.redirect(url ? url.original : 'http://localhost:3000');
    });

    app.post('/api/url', async (req, res) => {
        console.log(`Save URL for ${ req.body.originalUrl } ...`);

        let original = req.body.originalUrl;

        if (!validUrl.isUri(original)) {
            res.status(401).json('Invalid URL.');
        }

        try {
            let url = await urlCollection.addUrl(req.body.originalUrl);
            res.status(200).json(url);
        }
        catch (err) {
            console.log('Error occurred.', err);
            res.status(401).json('Server unavailable.');
        }
    });
};