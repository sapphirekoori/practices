var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var dbConfig = require('./database/config');

mongoose.Promise = global.Promise;
mongoose.connect(
    dbConfig.url,
    {
        useNewUrlParser : true,
        keepAlive: true,
        reconnectTries: 10
    },
    (error, db) => {
        if (error) {
            console.log('Failed to connect mongodb', error);
        }
        else {
            console.log('Connected to mongodb.');
        }
    }
);

// initial mongoose schema
require('./models/shortenedUrl');

var app = express();

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,x-access-token,X-Key');
    if (req.method == 'OPTIONS') {
        res.status(200).end();
    }
    else {
        next();
    }
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// add routers
require('./routes/base')(app);
require('./routes/url')(app);

// listen to port
let port = process.env.port || 3000;
app.listen(port, () => {
    console.log('Started on port 3000.');
});